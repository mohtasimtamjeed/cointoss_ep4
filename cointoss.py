import random

#output i in first column, heads or tails as 1 or -1 in second column, check the other column outputs
#check how to do file io using python
def coinToss(number):

     heads, tails = 1, -1 # multiple assignment
     
     data, displacement, meansquared = "", 0, 0

     f = open("cointoss.csv", "a")

     for i in range(number): # do this 'number' amount of times
          flip = random.randint(0, 1)
          if (flip == 0):
               displacement += heads
               meansquared += displacement**2
               f.write(f"{i+1},{heads},{displacement},{meansquared}\n")
               data += f"{i+1},{heads},{displacement},{meansquared}\n"
          else:
               displacement += tails
               meansquared += displacement**2
               f.write(f"{i+1},{tails},{displacement},{meansquared}\n")
               data += f"{i+1},{tails},{displacement},{meansquared}\n"
     print(data)
     f.close()
    

coinToss(100)