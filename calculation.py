import csv
import random

filename = "ep4-task1-frederik-koch.csv"          

rawdata = tuple((bin(random.getrandbits(200))))     #generates a tuple with random ones and zeros
row1 = rawdata[5:105:1]
x=0
y=0
plot = []

for i in row1:
    if int(i)==0:                                   #converts zeros to -1
        i=-1
    x += int(i)
    y += x**2
    a = [x,y]
    plot.append(a)
 
myFile = open(filename, 'w')   
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(plot)
     
print("Writing complete")
