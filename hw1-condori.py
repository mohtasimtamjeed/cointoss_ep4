import random

c = d = 0
with open('ep4-task1-hector-condori.cvs', 'w') as f:
  for a in range(1, 101):
    b = random.choice([-1, 1])
    c += b
    d += c*c
    f.write(f'{a}, {b}, {c}, {d}\n')
